package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin //解决跨域
public class EduLoginController {
    //login
    @PostMapping("login")
    public R login(String username,String password) {
        if (username.equals("admin") && password.equals("admin")){
            return R.ok().data("token", "admin");
        } else return R.error().message("密码错误");
    }

    //info
    @GetMapping("info")
    public R info() {
        return R.ok().data("roles", "[admin]").data("name", "admin").data("avatar", "https://img03.mifile.cn/v1/MI_542ED8B1722DC/83ea201885e58c9fc966005fecbf8304.jpg");

    }

    //logout
    @PostMapping("logout")
    public R logout() {
        return R.ok();
    }
}
