package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.entity.subject.TwoSubject;
import com.atguigu.eduservice.listener.SubjectExcelListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.reflection.wrapper.BaseWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author RockCode
 * @since 2021-02-24
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void saveSubject(MultipartFile file,EduSubjectService subjectService) {
        try {
            //文件输入流
            InputStream in = file.getInputStream();
            //调用方法进行读取
            EasyExcel.read(in, SubjectData.class, new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        //查出所有一级分类
        wrapperOne.eq("parent_id", "0");
        List<EduSubject> OneList = baseMapper.selectList(wrapperOne);

        //查出所有的二级分类
        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperTwo.ne("parent_id", "0");
        List<EduSubject> TwoList = baseMapper.selectList(wrapperTwo);

        //创建最终返回list
        List<OneSubject> finalSubject = new ArrayList<>();

        //封装一级分类，EduSubject变成OneSubject
        //从OneList中把值一个个取出来，然后封装到OneSubject这个实体类去
        for (int i = 0; i < OneList.size(); i++) {
            EduSubject eduSubject = OneList.get(i);
            OneSubject oneSubject = new OneSubject();
//            oneSubject.setId(eduSubject.getId());
//            oneSubject.setId(eduSubject.getTitle());
            //工具类实现封装
            BeanUtils.copyProperties(eduSubject, oneSubject);
            finalSubject.add(oneSubject);

            List<TwoSubject> twoSubjects = new ArrayList<>();
            //封装二级分类，EduSubject变成TwoSubject,然后把TwoSubject封装到List<TwoSubject>,再把List<TwoSubject>添加到OneSubject
            for (int j = 0; j < TwoList.size(); j++) {
                EduSubject eduSubject2 = TwoList.get(j);
                //把二级分类放入对应的一级分类中
                if (eduSubject2.getParentId().equals(oneSubject.getId())) {
                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(eduSubject2,twoSubject);

                    twoSubjects.add(twoSubject);
                }
            }
            oneSubject.setChildren(twoSubjects);
        }
        return finalSubject;
    }
}
